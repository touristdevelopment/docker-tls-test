﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;

namespace PingService
{
    class Program
    {
        static void  Main(string[] args)
        {
            Console.WriteLine("Starting Pinging");
            for(int i = 0; i < 10; i++) //Testing purpose only
            {
                Console.Write("STARTING");
                var task = Task.Run(async () => await ApiCall());
                task.GetAwaiter().GetResult();
                Console.Write("ENDING");
            }

        }
        private static async Task ApiCall()
        {
            try
            {
                HttpClient client = new HttpClient();

                string url = "http://webapp/weatherforecast"; //HVAD FANDEN SKAL URLEN VÆRE HER FOR JEG KAN FÅ DET TIL AT FUNGERE MED SSL?
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);

                HttpResponseMessage response = await client.SendAsync(request);

                string message = await response.Content.ReadAsStringAsync();

                Console.WriteLine(message);
                
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
